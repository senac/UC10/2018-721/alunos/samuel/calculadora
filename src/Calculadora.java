
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Timotei
 */
public class Calculadora {

    public static double soma(double a, double b) {
        return a + b;
    }

    public static double subtrcao(double a, double b) {
        return a - b;
    }

    public static double multiplicacao(double a, double b) {
        return a * b;
    }

    public static double dividir(double a, double b) {
        return a / b;
    }

    public static double lervalor(String mensagem) {
        String v;
        v = JOptionPane.showInputDialog(mensagem);
        return Double.parseDouble(v);
    }

    public static void mostrarResultado(double mr) {
        JOptionPane.showMessageDialog(null, "Resultado: " + mr);
    }

    public static void main(String[] args) {
        double numero1;
        double numero2;
        double resultado;
        int op;
        String v;

        String menu = "############# Calculadora #############\n"
                + "1 - Soma\n"
                + "2 - Subtração\n"
                + "3 - Multiplicação\n"
                + "4 - Divisão\n"
                + "0 - Sair\n"
                + "Entre com a opção";

        do {
            v = JOptionPane.showInputDialog(menu);
            op = Integer.parseInt(v);

            switch (op) {
                case 1:

                    numero1 = lervalor("Digite o Numer");
                    numero2 = lervalor("Digite o Numero");
                    resultado = soma(numero1, numero2);
                    mostrarResultado(resultado);

                    break;

                case 2:

                    numero1 = lervalor("Digite o Numer");
                    numero2 = lervalor("Digite o Numero");
                    resultado = subtrcao(numero1, numero2);
                    mostrarResultado(resultado);

                    break;

                case 3:

                    numero1 = lervalor("Digite o Numer");
                    numero2 = lervalor("Digite o Nnumero");
                    resultado = dividir(numero1, numero2);
                    mostrarResultado(resultado);

                    break;

                case 4:

                    numero1 = lervalor("Digite o Numer");
                    numero2 = lervalor("Digite o Numero");
                    resultado = multiplicacao(numero1, numero2);
                    mostrarResultado(resultado);

                    break;

                case 0:
                    System.exit(0);
                    break;

                default:
                    showmensagemerro("Opção Invalida, Tente Novamente Por Favor!");
            }

        } while (true);
    }    
        
    public static void showmensagemerro(String m) {
        JOptionPane.showMessageDialog(null, m, "ERRO", JOptionPane.ERROR_MESSAGE);
    }
  
        
}

