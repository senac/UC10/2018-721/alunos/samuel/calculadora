
import java.util.Scanner;

public class Terminal {

    public static double soma(double a, double b) {
        return a + b;
    }

    public static double subtrcao(double a, double b) {
        return a - b;
    }

    public static double multiplicacao(double a, double b) {
        return a * b;
    }

    public static double dividir(double a, double b) {
        return a / b;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        double numero1;
        double numero2;
        double resultado;
        int op;
        String v;

        String menu = "############# Calculadora #############\n"
                + "1 - Soma\n"
                + "2 - Subtração\n"
                + "3 - Multiplicação\n"
                + "4 - Divisão\n"
                + "0 - Sair\n"
                + "Entre com a opção";

        do {
            System.out.println(menu);

            op = scanner.nextInt();

            switch (op) {
                case 1:
                    System.out.println("Digite o Numero:");
                    numero1 = scanner.nextDouble();
                    System.out.println("Digite o Numero:");
                    numero2 = scanner.nextDouble();
                    resultado = soma(numero1, numero2);
                    System.out.println("Resultado:" + resultado);
                    break;

                case 2:
                    System.out.println("Digite o Numero:");
                    numero1 = scanner.nextDouble();
                    System.out.println("Digite o Numero:");
                    numero2 = scanner.nextDouble();
                    resultado = subtrcao(numero1, numero2);
                    System.out.println("Resultado:" + resultado);
                    break;

                case 3:
                    System.out.println("Digite o Numero:");
                    numero1 = scanner.nextDouble();
                    System.out.println("Digite o Numero:");
                    numero2 = scanner.nextDouble();
                    resultado = dividir(numero1, numero2);
                    System.out.println("Resultado:" + resultado);
                    break;

                case 4:
                    System.out.println("Digite o Numero:");
                    numero1 = scanner.nextDouble();
                    System.out.println("Digite o Numero:");
                    numero2 = scanner.nextDouble();
                    resultado = multiplicacao(numero1, numero2);
                    System.out.println("Resultado:" + resultado);
                    break;

                case 0:
                    System.exit(0);
                    break;
                default:
                    System.out.println("Opção Invalida, Por Favor Tente Novamente");

            }
        } while (true);
        

    }
}
